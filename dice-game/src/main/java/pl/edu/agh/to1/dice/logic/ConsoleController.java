package pl.edu.agh.to1.dice.logic;

import java.io.IOException;
import java.util.Scanner;

import pl.edu.agh.to1.dice.figures.Figures;

public class ConsoleController {
	
	private static DiceGame game;
	private Figures figures;
	private static Scanner scanner = new Scanner(System.in);
	
	private ConsoleGameConfigurator configurator = new ConsoleGameConfigurator();
	private ConsoleView view = new ConsoleView();
	
	public void configureGame()
	{
		figures = new Figures();
		ConsoleController.game = configurator.configure(figures);
	}
	
	public void playGame()
	{
		for(int turn = 0; turn < game.getTurns(); turn++)
		{
			for(Player player: game.getPlayers())
		
			{
				game.resetDices();
				
				playTurn(player, turn);
			}
	
		}
		view.viewSumup(game.getPlayers());
	}
	
	public void playTurn(Player player, int turn)
	{
		view.initTurn(player.getName(), turn);
		
		for (int roll = 0; roll < game.getRerollsCount(); roll++)
		{
		player.throwDices(game.getDiceSet());
		view.viewResult(game.getDiceSet());
		view.viewPoints(figures.getScoresForRoll(game.getDiceSet()));
		if (roll != game.getRerollsCount())
			view.askForKeep(game.getDiceSet());
		}
		
		view.print("########################");
		view.viewResult(game.getDiceSet());
		//full hd ftw
		int chosenFigure = view.viewPlayerScoreTableAndBenefits(player.getScoreTable(), figures.getScoresForRoll(game.getDiceSet()));
		player.getScoreTable().useFigure(chosenFigure, figures.getScoresForRoll(game.getDiceSet()).scores.get(chosenFigure));
		
		//general rules
		
		ScoreTable gameTable = figures.getScoresForRoll(game.getDiceSet());
		PlayerScoreTable playerTable = player.getScoreTable();
		int generalPosition = gameTable.scores.size() - 3;
		if (gameTable.scores.get(generalPosition) != 0 && playerTable.used.get(generalPosition) == true )
		{
			int bonusPosition = gameTable.scores.size()-1;
			int bonus = gameTable.scores.get(bonusPosition);
			playerTable.scores.set(bonusPosition, bonus + 100);
		}
		
	}
}

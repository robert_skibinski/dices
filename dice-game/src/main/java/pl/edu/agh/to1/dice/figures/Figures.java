package pl.edu.agh.to1.dice.figures;

import java.util.ArrayList;
import java.util.List;

import pl.edu.agh.to1.dice.logic.DiceSet;
import pl.edu.agh.to1.dice.logic.PlayerScoreTable;
import pl.edu.agh.to1.dice.logic.ScoreTable;

public class Figures {

	private List<Figure> figures = new ArrayList<Figure>();
	protected String[] upNames = {"jedynki", "dwójki", "trójki", "czwórki", "piątki", "szóstki"};
	public Figures()
	{
		for(int i=1;i<=6;i++){
			figures.add(new UpTable(upNames[i-1], i));
		}	
		figures.add(new ThreeSame("3 jednakowe"));
		figures.add(new FourSame("4 jednakowe"));
		figures.add(new Full("Full"));
		figures.add(new SmallStreet("Mały street"));
		figures.add(new BigStreet("Duży street"));
		figures.add(new General("Generał"));
		figures.add(new Chance("Szansa"));
		figures.add(new Bonus("Premia"));

	}

	public ScoreTable getScoresForRoll(DiceSet diceSet)
	{
		ScoreTable scores = new ScoreTable();
		for(Figure f: figures){
			scores.figureNames.add(f.getName());
			scores.scores.add(f.getResult(diceSet));
		}
		return scores;
	}

	public PlayerScoreTable getEmptyScoreTable()
	{
		PlayerScoreTable scoreTable = new PlayerScoreTable();
		for(Figure f: figures){
			scoreTable.figureNames.add(f.getName());
			scoreTable.scores.add(0);
			scoreTable.used.add(false);
		}
		return scoreTable;
	}
}

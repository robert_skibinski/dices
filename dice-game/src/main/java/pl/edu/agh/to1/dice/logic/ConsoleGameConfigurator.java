package pl.edu.agh.to1.dice.logic;

import java.util.Scanner;

import pl.edu.agh.to1.dice.figures.Figures;

public class ConsoleGameConfigurator {

	private Scanner scanner = new Scanner(System.in);
	private GameBuilder gameBuilder = new GameBuilder();
	
	
	
	public DiceGame configure(Figures figures){
		String input;
		
		System.out.println("DiceGame configuration:");
		System.out.println("How many players would like to play?");
		int player_count = scanner.nextInt();
		for (int i=0;i<player_count;i++)
		{
			System.out.format("Please enter %d player name:\n", i+1);
			input = scanner.next();
			gameBuilder.addPlayer(input, figures);
		}
		
		return gameBuilder.create();
		
	}
	
}

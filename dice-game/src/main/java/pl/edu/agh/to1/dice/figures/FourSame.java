package pl.edu.agh.to1.dice.figures;

import pl.edu.agh.to1.dice.logic.Dice;
import pl.edu.agh.to1.dice.logic.DiceSet;

 class FourSame extends CountingFigure {

	public FourSame(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	@Override
	public int getResult(DiceSet diceSet) {
		count(diceSet);
		
		int result = 0;
		for (Dice d: diceSet.dices){
			result += d.getValue();
		}
		for (int i = 0; i < 6; i++)
		{
			if (counter[i] == 4)
				return result;
		}
		return 0;
		
	}
}

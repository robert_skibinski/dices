package pl.edu.agh.to1.dice.figures;

import pl.edu.agh.to1.dice.logic.DiceSet;

abstract class Figure {
	private String name;
	
	public Figure(String name)
	{
		this.name = name;
	}
	
	public abstract int getResult(DiceSet diceSet);
	
	public String getName()
	{
		return this.name;
	}
}

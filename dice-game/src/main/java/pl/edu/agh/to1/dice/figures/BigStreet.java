package pl.edu.agh.to1.dice.figures;

import pl.edu.agh.to1.dice.logic.DiceSet;

public class BigStreet extends CountingFigure {

	public BigStreet(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	@Override
	public int getResult(DiceSet diceSet) {
		count(diceSet);
		int singles = 0;
		
		for(int i=0; i<6; i++)
		{
			if (counter[i] == 1)
				singles++;
		}
		if ( singles == 5)
			return 40;
		
		return 0;
	}

}

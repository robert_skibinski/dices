package pl.edu.agh.to1.dice.logic;

import java.util.Random;

public final class Dice {
	
	private int value;
	private boolean isKept;
	
	private static Random generator = new Random();
	
	Dice()
	{
		value = 0;
		isKept = false;
	}
	
	public void roll()
	{
		if (isKept == false)
			this.value = (generator.nextInt(6) +1);
	}
	
	public void switchKeep()
	{
		isKept ^= true; //funny funny xor operator.
	}
	public void resetKeep()
	{
		isKept = false;
	}
	
	
	public int getValue()
	{
		return this.value;
	}
	
	public boolean isKept()
	{
		return this.isKept;
	}
	
}

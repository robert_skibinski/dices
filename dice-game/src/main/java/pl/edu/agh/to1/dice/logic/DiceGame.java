package pl.edu.agh.to1.dice.logic;

import java.util.List;

import pl.edu.agh.to1.dice.figures.Figures;

public class DiceGame {

	private List<Player> players;
	private int turns;
	private int actualTurn;
	private int rerollsCount;
	private DiceSet diceSet;
	
	private Figures figures;
	
	public DiceGame(List<Player> players2, int turns, int diceCount, int rerollsCount)
	{
		this.players = players2;
		this.turns = turns;
		this.actualTurn = 0;
		this.rerollsCount = rerollsCount;
		this.diceSet = new DiceSet(diceCount);
		
		this.figures = new Figures();
		
	}
	
	public void resetDices(){
		for(Dice d: diceSet.dices)
		{
			d.resetKeep();
		}
	}
	public List<Player> getPlayers() {
		return players;
	}

	public int getTurns() {
		return turns;
	}

	public int getActualTurn() {
		return actualTurn;
	}
	
	public void nextTurn()
	{
		this.actualTurn++;
	}

	public DiceSet getDiceSet() {
		return diceSet;
	}

	public int getRerollsCount() {
		return rerollsCount;
	}

	public Figures getFigures() {
		return figures;
	}
}

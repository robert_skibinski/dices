package pl.edu.agh.to1.dice.logic;

import pl.edu.agh.to1.dice.figures.Figures;

public class HumanPlayer extends Player {

	private String name;
	private PlayerScoreTable scoreTable;
	
	public HumanPlayer(String name, Figures figures)
	{
		this.name = name;
		this.scoreTable = figures.getEmptyScoreTable();
	}
	
	public void throwDices(DiceSet ds) {
		for(Dice dice: ds.dices)
		{
			dice.roll();
		}

	}

	public void chooseDicesToKeep(DiceSet ds) {
		// TODO Auto-generated method stub
		
	}

	public String getName()
	{
		return this.name;
	}

	public PlayerScoreTable getScoreTable() {
		return scoreTable;
	}

	
	

}

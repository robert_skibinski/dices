package pl.edu.agh.to1.dice.figures;

import pl.edu.agh.to1.dice.logic.DiceSet;

 class SmallStreet extends CountingFigure {

	public SmallStreet(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	@Override
	public int getResult(DiceSet diceSet) {
		count(diceSet);
		int singles = 0, doubles = 0;
		
		for(int i=0; i<6; i++)
		{
			if (counter[i] == 1)
				singles++;
			if (counter[i] == 2)
				doubles++;
		}
		if ( (singles == 4) || (singles == 3 && doubles == 1) )
			return 30;
		
		return 0;
	}

}

package pl.edu.agh.to1.dice.logic;

import java.util.ArrayList;
import java.util.List;

import pl.edu.agh.to1.dice.figures.Figures;

public class GameBuilder {
	
	private List<Player> players = new ArrayList<Player>();
	
	private final int TURNS = 13;
	private final int DICE_COUNT = 5;
	private final int REROLLS_COUNT = 2;
	
	private int diceCount = DICE_COUNT;
	private int turns = TURNS;
	private int rerollsCount = REROLLS_COUNT;
	public GameBuilder addPlayer(String name, Figures figures)
	{
		players.add(new HumanPlayer(name, figures));
		return this;
	}

	public DiceGame create() {
		return new DiceGame(players, turns, diceCount, rerollsCount);
	}
}

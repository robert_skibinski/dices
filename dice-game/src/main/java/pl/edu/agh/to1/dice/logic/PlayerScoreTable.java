package pl.edu.agh.to1.dice.logic;

import java.util.ArrayList;

public class PlayerScoreTable extends ScoreTable {

	public ArrayList<Boolean> used;
	public PlayerScoreTable() {
		super();
		used = new ArrayList<Boolean>();
		
	
	}
	/**
	 * Specially for all kind of kinky rules, such as for generals-recycled. That's ugly.
	 * @param index
	 * @param value
	 */
	public void useFigure(int index, int value)
	{
		scores.set(index, value);
		used.set(index, true);
		
		
	}
	
	public int sum()
	{
		int sum = 0;
		for (int i = 0; i<scores.size(); i++)
			sum += scores.get(i);
		return sum;
	}
}

package pl.edu.agh.to1.dice.figures;

import pl.edu.agh.to1.dice.logic.Dice;
import pl.edu.agh.to1.dice.logic.DiceSet;

class Chance extends Figure {

	public Chance(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	@Override
	public int getResult(DiceSet diceSet) {
		int result = 0;
		for(Dice d: diceSet.dices)
			result += d.getValue();
		return result;
	}

}

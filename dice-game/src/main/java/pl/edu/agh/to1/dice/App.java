package pl.edu.agh.to1.dice;

import pl.edu.agh.to1.dice.logic.ConsoleController;

public class App {
	
    public static void main(String[] args) {

      ConsoleController controller = new ConsoleController();
      controller.configureGame();
      controller.playGame();
      
      
    }
}

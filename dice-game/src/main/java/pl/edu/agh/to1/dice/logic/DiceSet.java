package pl.edu.agh.to1.dice.logic;

import java.util.ArrayList;

public class DiceSet {
	
	public ArrayList<Dice> dices;
	
	DiceSet(int diceNumber){
		dices = new ArrayList<Dice>(diceNumber);
		
		for(int i = 0; i < diceNumber; i++)
			dices.add(new Dice());
		
	}
	
	
}

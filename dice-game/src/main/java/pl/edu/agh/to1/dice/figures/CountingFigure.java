package pl.edu.agh.to1.dice.figures;

import pl.edu.agh.to1.dice.logic.Dice;
import pl.edu.agh.to1.dice.logic.DiceSet;


abstract class CountingFigure extends Figure {

	int[] counter;
	public CountingFigure(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}
	
	protected void count(DiceSet diceSet)
	{
		counter = new int[6];
		for (Dice d: diceSet.dices){
			counter[d.getValue()-1]++;
		}
	}

	

}

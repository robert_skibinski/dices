package pl.edu.agh.to1.dice.figures;

import pl.edu.agh.to1.dice.logic.Dice;
import pl.edu.agh.to1.dice.logic.DiceSet;

 class UpTable extends Figure {

	private int number;
	public UpTable(String name, int number) {
		super(name);
		this.number = number;
		// TODO Auto-generated constructor stub
	}

	@Override
	public int getResult(DiceSet diceSet) {
		int result = 0;
		for(Dice d: diceSet.dices){
			if(d.getValue() == number)
				result++;
		}
		return result*number;
	}

}

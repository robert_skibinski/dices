package pl.edu.agh.to1.dice.figures;

import pl.edu.agh.to1.dice.logic.DiceSet;

 class Full extends CountingFigure {

	public Full(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	@Override
	public int getResult(DiceSet diceSet) {
		count(diceSet);
		boolean isThree = false, isTwo = false;
		
		for(int i = 0; i<diceSet.dices.size(); i++)
		{
			if (counter[i] == 2)
				isTwo = true;
			if (counter[i] == 3)
				isThree = true;
		}
		if(isTwo == true && isThree == true){
			return 25;
		}
		return 0;
	}

}

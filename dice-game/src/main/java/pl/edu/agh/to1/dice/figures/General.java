package pl.edu.agh.to1.dice.figures;

import pl.edu.agh.to1.dice.logic.DiceSet;

 class General extends CountingFigure {

	public General(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	@Override
	public int getResult(DiceSet diceSet) {
		count(diceSet);
		
		for(int i=0;i<6;i++)
			if (counter[i] == 5)
				return 5*(i+1);
		return 0;
	}

}

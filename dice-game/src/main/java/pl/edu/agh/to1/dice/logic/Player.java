package pl.edu.agh.to1.dice.logic;

public abstract class Player implements Comparable<Player>{

	public abstract void throwDices(DiceSet ds);
	public abstract void chooseDicesToKeep(DiceSet ds);
	public abstract String getName();
	public abstract PlayerScoreTable getScoreTable();
	
	public int compareTo(Player arg0) {
		if (this.getScoreTable().sum() < arg0.getScoreTable().sum())
			return -1;
		else if(this.getScoreTable().sum() > arg0.getScoreTable().sum())
			return 1;
		return 0;
	}
}

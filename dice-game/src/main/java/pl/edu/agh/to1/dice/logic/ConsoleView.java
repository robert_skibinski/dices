package pl.edu.agh.to1.dice.logic;

import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class ConsoleView {
	private static Scanner scanner = new Scanner(System.in);
	
	public void initTurn(String playerName, int actualTurn)
	{
		System.out.format("Now it's %s player %d turn!\n", playerName, actualTurn+1);
		
	}

	public void viewResult(DiceSet diceSet) {
		System.out.println("Throw result:");
		String ifBlocked = "";
		for (int i = 0; i<diceSet.dices.size(); i++)
		{
			if (diceSet.dices.get(i).isKept() == true)
				ifBlocked = "Kept";
			else 
				ifBlocked =	"";
			System.out.format("#%d.\t%d\t%s\n", i+1, diceSet.dices.get(i).getValue(), ifBlocked);
		}
	}

	public void viewPoints(ScoreTable scores) {
		System.out.println("Name\t\tScore\tUsed");
		
		for (int i=0; i<scores.figureNames.size(); i++)
		{

			System.out.format("%d.%s\t%d\n", i+1, scores.figureNames.get(i), scores.scores.get(i));
		}
		
	}

	public void askForKeep(DiceSet diceSet) {
		String cmd;
		int index;
		System.out.println("Enter numbers of dices to keep, # to confirm and proceed");
		do{
			cmd = scanner.next();
			if(cmd.equals("#")){
				break;
			}
			else{
				index = Integer.parseInt(cmd) - 1;
				diceSet.dices.get(index).switchKeep();
			}
		}while(true);
		System.out.println("After changes:");
		String ifBlocked = "";
		for (int i = 0; i<diceSet.dices.size(); i++)
		{
			if (diceSet.dices.get(i).isKept() == true)
				ifBlocked = "Kept";
			else 
				ifBlocked =	"";
			System.out.format("#%d.\t%d\t%s\n", i+1, diceSet.dices.get(i).getValue(), ifBlocked);
		}
		
	}

	public void getFigureChoice() {
		
		
	}

	public void print(String string) {
		System.out.println(string);
	}

	public int viewPlayerScoreTableAndBenefits(PlayerScoreTable playerTable, ScoreTable gameTable) {
		System.out.println("Please enter figure(only one!) of your choice:");
		
		String isUsed;
		int i;
		for(i=0; i<playerTable.figureNames.size(); i++)
		{
			if ( playerTable.used.get(i) == true)
				isUsed = "Used";
			else 
				isUsed = "";
			System.out.format("%d. %s\t%d + %d\t%s\n", (i+1), playerTable.figureNames.get(i), playerTable.scores.get(i), gameTable.scores.get(i), 
					isUsed);
			
		}
		
		return scanner.nextInt()-1;
		
	}

	public void viewSumup(List<Player> players) {
		Collections.sort(players);
		for(int i=0; i<players.size(); i++)
		{
			System.out.format("#%d. %s with \t %d points\n", i+1, players.get(i).getName(), players.get(i).getScoreTable().sum());
		}
		
	}

	
}
